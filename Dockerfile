FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

RUN apt-get update && \
	apt-get install -y build-essential golang && \
	rm -rf /var/cache/apt /var/lib/apt/lists

ENV BUILDTAGS include_oss include_gcs

WORKDIR /app/build
ARG REGISTRY_VERSION="v2.7.1"
RUN curl -L "https://github.com/docker/distribution/archive/${REGISTRY_VERSION}.tar.gz" \
    | tar -xz --strip-components=1 -C /app/build
RUN go get .
RUN CGO_ENABLED=0 make PREFIX=/go binaries && file ./bin/registry | grep "statically linked" && chmod +x ./bin/registry
RUN mkdir -p /app/code && cp ./bin/registry /app/code/

WORKDIR /app/code

RUN rm -rf /app/build
COPY config-example.yml htpasswd start.sh /app/pkg/

RUN chmod +x /app/pkg/start.sh

WORKDIR /app/data

CMD [ "/app/pkg/start.sh" ]

